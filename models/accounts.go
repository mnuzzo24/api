package models

import (
	"github.com/dgrijalva/jwt-go"
	u "api/utils"
	//"strings"
	"github.com/jinzhu/gorm"
	"os"
	"golang.org/x/crypto/bcrypt"
)

/*
JWT claims struct
*/
type Token struct {
	UserId uint
	jwt.StandardClaims
}

//a struct to rep partner account
type Account struct {
	gorm.Model
	Partner string `json:"partner"`
	Passcode string `json:"passcode"`
	Token string `json:"token";sql:"-"`
}

//Validate incoming partner details...
func (account *Account) Validate() (map[string] interface{}, bool) {

	if account.Partner == "" {
		return u.Message(false, "Partner is required"), false
	}

	if len(account.Passcode) < 6 {
		return u.Message(false, "Passcode is required"), false
	}

	//Partner must be unique
	temp := &Account{}

	//check for errors and duplicate partners
	err := GetDB().Table("accounts").Where("partner = ?", account.Partner).First(temp).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return u.Message(false, "Connection error. Please retry"), false
	}
	if temp.Partner != "" {
		return u.Message(false, "Partner is already in use."), false
	}

	return u.Message(false, "Requirement passed"), true
}

func (account *Account) Create() (map[string] interface{}) {

	if resp, ok := account.Validate(); !ok {
		return resp
	}

	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(account.Passcode), bcrypt.DefaultCost)
	account.Passcode = string(hashedPassword)

	GetDB().Create(account)

	if account.ID <= 0 {
		return u.Message(false, "Failed to create partner, connection error.")
	}

	//Create new JWT token for the newly registered account
	tk := &Token{UserId: account.ID}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))
	account.Token = tokenString

	account.Passcode = "" //delete passcode

	response := u.Message(true, "Partner has been created")
	response["account"] = account
	return response
}

func Login(partner, password string) (map[string]interface{}) {

	account := &Account{}
	err := GetDB().Table("accounts").Where("partner = ?", partner).First(account).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return u.Message(false, "Partner not found")
		}
		return u.Message(false, "Connection error. Please retry")
	}

	err = bcrypt.CompareHashAndPassword([]byte(account.Passcode), []byte(password))
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword { //Passcode does not match!
		return u.Message(false, "Invalid credentials. Please try again")
	}
	//Worked! Logged In
	account.Passcode = ""

	//Create JWT token
	tk := &Token{UserId: account.ID}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))
	account.Token = tokenString //Store the token in the response

	resp := u.Message(true, "Logged In")
	resp["account"] = account
	return resp
}

func GetUser(u uint) *Account {

	acc := &Account{}
	GetDB().Table("accounts").Where("id = ?", u).First(acc)
	if acc.Partner == "" { //User not found!
		return nil
	}

	acc.Passcode = ""
	return acc
}
