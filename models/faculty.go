package models

import (
	u "api/utils"
	"fmt"

	"github.com/jinzhu/gorm"
)

type Faculty struct {
	gorm.Model
	Netid string `json:"netid"`
}

/*
 This struct function validate the required parameters sent through the http request body
returns message and true if the requirement is met
*/
func (faculty *Faculty) Validate() (map[string]interface{}, bool) {

	//if faculty.Netid == "" {
	//	return u.Message(false, "Netid should be on the payload"), false
	//}

	//All the required parameters are present
	return u.Message(true, "success"), true
}

func GetFaculty() []*Faculty {

	faculty := make([]*Faculty, 0)
	err := GetDB().Table("faculty_id").Where("academic_flag = ?", "N").Find(&faculty).Error

	if err != nil {
		fmt.Println(err)
		return nil
	}

	return faculty
}
