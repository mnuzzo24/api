package main

import (
	"github.com/gorilla/mux"
	"api/app"
	"os"
	"fmt"
	"net/http"
	"api/controllers"
)

func main() {

	router := mux.NewRouter()

	router.HandleFunc("/api/auth/partner/new", controllers.CreatePartner).Methods("POST") // CreatePartner
	router.HandleFunc("/api/auth", controllers.Authenticate).Methods("POST") // Obtain JWT.
	router.HandleFunc("/api/fao/faculty", controllers.GetFacultyFor).Methods("GET") // Get all active faculty.

	router.Use(app.JwtAuthentication) //attach JWT auth middleware

	//router.NotFoundHandler = app.NotFoundHandler

	port := os.Getenv("PORT")
	if port == "" {
		port = "8000" //localhost
	}

	fmt.Println(port)

	err := http.ListenAndServe(":" + port, router) //Launch the app, visit localhost:8000/api
	if err != nil {
		fmt.Print(err)
	}
}